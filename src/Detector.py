'''
    File name         : detectors.py
    Description       : Object detector used for detecting the objects in a video /image
    Author            : Rahmad Sadli
    Date created      : 20/02/2020
    Python Version    : 3.7
'''

# Import python libraries
import numpy as np
import cv2


def detectBrightest(frame, threshold=200, debugMode=False):
    centers = np.unravel_index(np.argmax(frame), frame.shape)
    return np.flip(centers) if np.max(frame) > threshold else None


def detectThresholdErod(frame, threshold=200, debugMode=False):
    # img_hist_eq = cv2.equalizeHist(frame)
    # threshold=250
    thres = cv2.threshold(frame, thresh=threshold, maxval=255, type=cv2.THRESH_BINARY)[1]
    # kernel = np.ones((2, 2), np.uint8)
    kernel = np.array([[0, 1, 0],
                       [1, 1, 1],
                       [0, 1, 0]], np.uint8)
    erod = cv2.erode(thres, kernel=kernel, iterations=1)
    # erod = cv2.morphologyEx(thres, cv2.MORPH_OPEN, kernel=kernel)
    centers = np.unravel_index(np.argmax(erod), erod.shape)

    return np.flip(centers) if np.max(erod) > 0 else None


def detect(frame, debugMode):
    # Convert frame from BGR to GRAY
    # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = frame

    if (debugMode):
        cv2.imshow('gray', gray)

    # histogram equalization
    img_hist_eq = cv2.equalizeHist(gray)
    # img_hist_eq = gray
    if (debugMode):
        cv2.imshow('img_hist_eq', img_hist_eq)

    # Convert to black and white image
    ret, img_thresh = cv2.threshold(img_hist_eq, 254, 255, cv2.THRESH_BINARY)
    if (debugMode):
        cv2.imshow('img_thresh', img_thresh)

    # Find contours
    contours, _ = cv2.findContours(img_thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Set the accepted minimum & maximum radius of a detected object
    min_radius_thresh= 3
    max_radius_thresh= 30

    centers=[]
    for c in contours:
        # ref: https://docs.opencv.org/trunk/dd/d49/tutorial_py_contour_features.html
        (x, y), radius = cv2.minEnclosingCircle(c)
        radius = int(radius)

        #Take only the valid circle(s)
        if (radius > min_radius_thresh) and (radius < max_radius_thresh):
            centers.append(np.array([[x], [y]]))
    cv2.imshow('contours', img_thresh)
    return centers



