import os
import glob
# from torch.utils.data import Dataset
# from torchvision import transforms
import cv2
import numpy as np


def video2numpy(path):
    """Loads a video as numpy array
    
    path: (str) Path to video
    reutrn: (np.array) video as numpy array dim:(F, H, W) (frames, height, width)
    """
    cap = cv2.VideoCapture(path)
    frame_lst = []
    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame_lst.append(frame)
        else:
            break
    cap.release()
    video = np.stack(frame_lst)
    return video


# class videoDataset(Dataset):
#     def __init__(self, data_dir='data_target_tracking', transforms=transforms.Compose([]) , preload=False):
#         """Loads videos from a folder an provides them as numpy array
#
#         data_dir: (str) folder with videos
#         preload: (bool) If True loads all videos at the beginning (very memory intense)
#         """
#         self.data_dir = data_dir
#         self.video_path_lst = glob.glob(os.path.join(data_dir, '*.mp4'), recursive=True)
#         self.preloaded = preload
#         self.transforms = transforms
#         if preload:
#             self.video_lst = [self.transforms(video2numpy(vid)) for vid in self.video_path_lst]
#
#     def __len__(self):
#         return len(self.video_path_lst)
#
#     def __getitem__(self, idx):
#         if self.preloaded:
#             return self.video_lst[idx]
#         else:
#             return self.transforms(video2numpy(self.video_path_lst[idx]))