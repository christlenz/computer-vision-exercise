import numpy as np
import cv2

import dataset
import Detector

if __name__ == "__main__":
    frames = dataset.video2numpy('../data/data_target_tracking/video_000.mp4')
    frame_rate = 1.0 / 30.0

    # # histogram equalization across entire video (takes too long)
    # frames_flat = frames.ravel()
    # frames_hist_eq = cv2.equalizeHist(frames_flat)
    # frames_hist_eq = frames_hist_eq.unravel(frames.shape)



    kalman_fil = cv2.KalmanFilter(4, 2)
    kalman_fil.measurementMatrix = np.array([[1, 0, 0, 0],
                                             [0, 1, 0, 0]], np.float32)
    kalman_fil.transitionMatrix = np.array([[1, 0, frame_rate, 0],
                                            [0, 1, 0, frame_rate],
                                            [0, 0, 1, 0],
                                            [0, 0, 0, 1]], np.float32)
    kalman_fil.processNoiseCov = np.array([[9, 0, 0, 0],
                                           [0, 9, 0, 0],
                                           [0, 0, 1, 0],
                                           [0, 0, 0, 1]], np.float32) * 1.0
    kalman_fil.measurementNoiseCov = np.array([[1, 0],
                                               [0, 1]], np.float32) * 3.0

    kalman_initialized = False
    kalman_init_state = None
    estimated_position = np.zeros((2,), dtype=np.float32)

    for frame_idx, frame in enumerate(frames):
        # detected_position = Detector.detect(frame, debugMode=True)
        detected_position = Detector.detectBrightest(frame, threshold=200)
        # detected_position = Detector.detectThresholdErod(frame, threshold=200)
        if detected_position is not None:
            measurement_position = detected_position.astype(np.float32)
            if not kalman_initialized:
                # kalman_fil.predict()
                kalman_init_position = measurement_position
                kalman_initialized = True
            min_outlier_distance = 50.0
            if np.sum((measurement_position - kalman_init_position - estimated_position)**2) < min_outlier_distance**2:
                kalman_fil.correct(measurement_position - kalman_init_position)
            else:
                print("OUTLIER")
        if kalman_initialized:
            estimated_state = kalman_fil.predict()
            estimated_position = estimated_state[0:2, 0]

        if detected_position is not None:
            frame = cv2.circle(frame, detected_position.astype(np.uint), radius=5, color=(255, 0, 0), thickness=2)
        if kalman_initialized:
            frame = cv2.circle(frame, (estimated_position + kalman_init_position).astype(np.uint), radius=10, color=(255, 0, 0), thickness=1)

        cv2.imshow('frame nr: {nr}'.format(nr=0), frame)
        cv2.waitKey(0)